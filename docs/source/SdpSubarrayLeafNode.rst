.. TMC Prototype documentation master file, created by
   sphinx-quickstart on Thu Jan 31 16:54:35 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SDP Subarray Leaf Node
========================================================

.. automodule:: tmcprototype.sdpsubarrayleafnode.src.sdpsubarrayleafnode.sdp_subarray_leaf_node
   :members:
   :undoc-members:
